/**
 * [exports description]
 * @type {[type]}
 */
module.exports = require(`./configs/webpack.${process.env.NODE_ENV || 'development'}.config`)
